#ifndef _TEXTURE_H_
#define _TEXTURE_H_

//Includes
#include <d3d11.h>
#include <atlbase.h>

class Texture
{
public:
	ATL::CComPtr<ID3D11Resource> mResource;
	ATL::CComPtr<ID3D11ShaderResourceView> mTexture;
	UINT mWidth;
	UINT mHeight;

public:
	Texture();
	~Texture();

	void InitializeParameters(ID3D11Resource* rsrce, ID3D11ShaderResourceView* rsrceView, UINT width, UINT height);
};

#endif