#include "Texture.h"

Texture::Texture(){

}

Texture::~Texture(){

}

void Texture::InitializeParameters(ID3D11Resource* rsrce, ID3D11ShaderResourceView* rsrceView, UINT width, UINT height) {
	mResource.Attach(rsrce);
	mTexture.Attach(rsrceView);
	mWidth = width;
	mHeight = height;
}