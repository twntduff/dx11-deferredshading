#ifndef _TIME_H_
#define _TIME_H_

#include <Windows.h>

class Time
{
private:
	LONGLONG mStart;
	float mFrequencySecs, mElapsedTime, mTotalTime;

public:
	Time();
	Time(const Time& time);
	~Time();

	bool Initialize();
	void Update();

	const float GetTime() const;
	const float GetDeltaTime() const;
};

#endif