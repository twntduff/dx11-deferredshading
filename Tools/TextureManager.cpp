#include "TextureManager.h"

TextureManager::TextureManager(){

}

TextureManager::~TextureManager(){

}

bool TextureManager::LoadAndRegisterTexture(ID3D11Device* device, std::string textureFile) {
	std::unique_ptr<Texture> texture(new Texture);
	ID3D11Resource* rsrce = nullptr;
	ID3D11ShaderResourceView* rsrceView = nullptr;
	UINT width, height;

	size_t reqLength = ::MultiByteToWideChar(CP_UTF8, 0, textureFile.c_str(), (int)textureFile.length(), 0, 0);
	std::wstring wFile(reqLength, L' ');
	std::copy(textureFile.begin(), textureFile.end(), wFile.begin());

	HRESULT result = DirectX::CreateWICTextureFromFile(device, wFile.c_str(), &rsrce, &rsrceView);
	if (FAILED(result)) {
		return false;
	}

	D3D11_RESOURCE_DIMENSION dim;
	rsrce->GetType(&dim);

	switch (dim) {
	case D3D11_RESOURCE_DIMENSION_TEXTURE2D:
	{
		D3D11_TEXTURE2D_DESC desc;
		auto tex = reinterpret_cast<ID3D11Texture2D*>(rsrce);

		tex->GetDesc(&desc);
		width = desc.Width; //width of texture in pixels
		height = desc.Height; //height of texture in pixels
	}break;

	default:
	{
		width = 0; //width of texture in pixels
		height = 0; //height of texture in pixels
	}
	}

	texture->InitializeParameters(rsrce, rsrceView, width, height);
	
	std::string key = textureFile.substr(9, textureFile.find(".", 0) - 9);
	texture.swap(mTextures[key]);
}

Texture* TextureManager::RetrieveTexture(const std::string key) {
	std::unordered_map<std::string, std::unique_ptr<Texture>>::const_iterator found = mTextures.find(key);

	if (found->second) {
		return found->second.get();
	}

	return nullptr;
}