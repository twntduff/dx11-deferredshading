#ifndef _MESH_H_
#define _MESH_H_

//Includes
#include <d3d11.h>
#include <atlbase.h>
#include <DirectXMath.h>
#include <vector>

struct VertexPTN {
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT2 Texture;
	DirectX::XMFLOAT3 Normal;
};

class Mesh
{
public:
	ATL::CComPtr<ID3D11Buffer> mIndexBuffer;
	ATL::CComPtr<ID3D11Buffer> mVertexBuffer;
	unsigned int mIndexCount;
	unsigned int mVertexCount;

public:
	Mesh();
	~Mesh();

	virtual bool InitializeBuffers(ID3D11Device* device, std::vector<VertexPTN> vertices, std::vector<UINT> indices);
	void RenderBuffers(ID3D11DeviceContext* deviceContext);
};

#endif