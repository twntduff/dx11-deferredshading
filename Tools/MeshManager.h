#ifndef _MESHMANAGER_H_
#define _MESHMANAGER_H_

//Includes
#include <unordered_map>
#include <memory>
#include <string>
#include <fstream>
#include "Mesh.h"

class MeshManager
{
private:
	std::unordered_map<std::string, std::unique_ptr<Mesh>> mMeshes;

public:
	MeshManager();
	~MeshManager();

	bool LoadAndRegisterMeshFromOBJ(ID3D11Device* device, const std::string meshFile);
	bool LoadAndRegisterCubeMesh(ID3D11Device* device);
	Mesh* RetrieveMesh(const std::string key);
};

#endif