#include "MeshManager.h"
#include "CubeMesh.h"

MeshManager::MeshManager(){

}

MeshManager::~MeshManager(){

}

bool MeshManager::LoadAndRegisterMeshFromOBJ(ID3D11Device* device, const std::string meshFile) {
	std::unique_ptr<Mesh> mesh(new Mesh);
	std::vector<DirectX::XMFLOAT3> positions;
	std::vector<DirectX::XMFLOAT2> texCoords;
	std::vector<DirectX::XMFLOAT3> normals;

	std::vector<VertexPTN> vertices;
	std::vector<UINT> indices;

	char cmd[256] = { 0 };

	std::ifstream file(meshFile);

	if (!file) {
		return false;
	}

	while (true) {
		file >> cmd;
		if (!file) {
			break;
		}

		if (strcmp(cmd, "#") == 0) {

		}
		else if (strcmp(cmd, "v") == 0) {
			float x, y, z;
			file >> x >> y >> z;

			positions.push_back(DirectX::XMFLOAT3(x, y, z));
		}
		else if (strcmp(cmd, "vt") == 0) {
			float u, v;
			file >> u >> v;

			texCoords.push_back(DirectX::XMFLOAT2(u, v));
		}
		else if (strcmp(cmd, "vn") == 0) {
			float x, y, z;
			file >> x >> y >> z;

			normals.push_back(DirectX::XMFLOAT3(x, y, z));
		}
		else if (strcmp(cmd, "f") == 0) {
			for (unsigned int i = 0; i < 3; i++) {
				bool found = false;
				UINT value;
				VertexPTN vertex;

				file >> value;
				vertex.Position = positions[value - 1];
				file.ignore();

				file >> value;
				vertex.Texture = texCoords[value - 1];
				file.ignore();

				file >> value;
				vertex.Normal = normals[value - 1];
				file.ignore();

				for (unsigned int j = 0; j < vertices.size(); j++) {
					if (vertex.Position.x == vertices[j].Position.x && vertex.Position.y == vertices[j].Position.y && vertex.Position.z == vertices[j].Position.z &&
						vertex.Texture.x == vertices[j].Texture.x && vertex.Texture.y == vertices[j].Texture.y &&
						vertex.Normal.x == vertices[j].Normal.x && vertex.Normal.y == vertices[j].Normal.y && vertex.Normal.z == vertices[j].Normal.z) {
						indices.push_back(j);
						found = true;
					}
				}

				if (!found) {
					vertices.push_back(vertex);
					indices.push_back(vertices.size() - 1);
				}
			}
		}
	}

	file.close();

	if (!mesh->InitializeBuffers(device, vertices, indices)) {
		return false;
	}

	std::string key = meshFile.substr(10, meshFile.find(".", 0) - 10);
	//std::string key = meshFile.substr(0, meshFile.find(".",0));

	mesh.swap(mMeshes[key]);

	return true;
}

bool MeshManager::LoadAndRegisterCubeMesh(ID3D11Device* device) {
	std::vector<VertexPTN> vertices;
	std::vector<UINT> indices;

	std::unique_ptr<Mesh> mesh(new CubeMesh);

	if (!mesh->InitializeBuffers(device, vertices, indices)) {
		return false;
	}

	std::string key = "Cube";

	mesh.swap(mMeshes[key]);

	return true;
}

Mesh* MeshManager::RetrieveMesh(const std::string key) {
	std::unordered_map<std::string, std::unique_ptr<Mesh>>::const_iterator found = mMeshes.find(key);

	if (found->second) {
		return found->second.get();
	}

	return nullptr;
}