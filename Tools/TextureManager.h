#ifndef _TEXTUREMANAGER_H_
#define _TEXTUREMANAGER_H_

//Includes
#include <WICTextureLoader.h>
#include <string>
#include <memory>
#include <unordered_map>
//#include <algorithm>
//#include <ctype.h>
#include "Texture.h"

class TextureManager
{
private:
	std::unordered_map<std::string, std::unique_ptr<Texture>> mTextures;

public:
	TextureManager();
	~TextureManager();

	bool LoadAndRegisterTexture(ID3D11Device* device, std::string textureFile);
	Texture* RetrieveTexture(const std::string key);
};

#endif