#include "System.h"

System::System(){
	mInput = 0;
	mGame = 0;
}

System::System(const System& other) {
}

System::~System(){
	//Shutdown();
}

//The following Initialize function does all the setup for the application. It first calls 
//InitializeWindows which will create the window for our application to use. It also creates 
//and initializes both the input and graphics objects that the application will use for handling 
//user input and rendering graphics to the screen.
bool System::Initialize(){
	int screenWidth, screenHeight;
	bool result;

	// Initialize the width and height of the screen to zero before sending the variables into the function.
	screenWidth = 0;
	screenHeight = 0;

	// Initialize the windows api.
	if (!InitializeWindows(screenWidth, screenHeight)) {
		return false;
	}

	// Create the graphics object. This object will handle rendering all the graphics for this application.
	mGame.reset(new Game);
	if (!mGame){
		return false;
	}

	// Initialize the graphics object.
	result = mGame->Initialize(screenWidth, screenHeight, mHwnd);
	if (!result){
		return false;
	}

	mTime.reset(new Time);
	if (!mTime) {
		return false;
	}

	result = mTime->Initialize();
	if (!result){
		return false;
	}

	// Create the input object. This object will be used to handle reading the keyboard input from the user.
	mInput.reset(new Input);
	if (!mInput) {
		return false;
	}

	// Initialize the input object.
	if (!mInput->Initialize(mHInstance, mHwnd)) {
		return false;
	}
	
	mGame->LoadTestLevel(mInput.get(), mHwnd);

	return true;
}

//The Shutdown function does the clean up. It shuts down and releases everything 
//associated with the graphics and input object. As well it also shuts down the window 
//and cleans up the handles associated with it.
void System::Shutdown(){
	// Shutdown the window.
	ShutdownWindows();

	return;
}

//The Run function is where our application will loop and do all the application 
//processing until we decide to quit. The application processing is done in the 
//Frame function which is called each loop.
void System::Run(){
	MSG msg;
	bool done, result;

	// Initialize the message structure.
	ZeroMemory(&msg, sizeof(MSG));
	
	// Loop until there is a quit message from the window or the user.
	done = false;
	while(!done){
		// Handle the windows messages.
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)){
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// If windows signals to end the application then exit out.
		if(msg.message == WM_QUIT){
			done = true;
		}
		else{
			// Otherwise do the frame processing.
			result = Frame();
			if(!result){
				done = true;
			}
		}
	}

	return;
}

//The Frame function is where all the processing for our application is done.
bool System::Frame(){
	bool result;

	mTime->Update();

	mInput->CheckDevice(mHwnd);

	// Check if the user pressed escape and wants to exit the application.
	if (mInput->IsKeyPressed(DIK_ESCAPE)){
		Shutdown();
		return false;
	}

	// Do the frame processing for the graphics object.
	result = mGame->Frame(mTime->GetDeltaTime());
	if (!result){
		return false;
	}

	return true;
}

//The MessageHandler function is where we direct the windows system messages into. 
//This way we can listen for certain information that we are interested in.
LRESULT CALLBACK System::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam){
	switch (umsg){
		// Check if a key has been pressed on the keyboard.
	case WM_KEYDOWN:{
		// If a key is pressed send it to the input object so it can record that state.
		mInput->KeyPressed((unsigned int)wparam);
		return 0;
	}

	// Check if a key has been released on the keyboard.
	//case WM_KEYUP:{
	//	// If a key is released then send it to the input object so it can unset the state for that key.
	//	mInput->KeyPressed((unsigned int)wparam);
	//	return 0;
	//}

	// Any other messages send to the default message handler as our application won't make use of them.
	default:{
		return DefWindowProc(hwnd, umsg, wparam, lparam);
	}
	}
}

//The InitializeWindows function is where we put the code to build the window we will 
//use to render to. It returns screenWidth and screenHeight back to the calling function 
//so we can make use of them throughout the application.
bool System::InitializeWindows(int& screenWidth, int& screenHeight){
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;

	// Get an external pointer to this object.	
	ApplicationHandle = this;

	// Get the instance of this application.
	mHInstance = GetModuleHandle(NULL);

	// Give the application a name.
	mApplicationName = L"Engine";

	// Setup the windows class with default settings.
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = mHInstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = mApplicationName;
	wc.cbSize = sizeof(WNDCLASSEX);

	// Register the window class.
	RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	screenWidth = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if (FULL_SCREEN){
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = (unsigned long)screenWidth;
		dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;
	}
	else{
		// If windowed then set it to 800x600 resolution.
		screenWidth = SCREEN_WIDTH;
		screenHeight = SCREEN_HEIGHT;

		// Place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

	// Create the window with the screen settings and get the handle to it.
	mHwnd = CreateWindowEx(WS_EX_APPWINDOW, mApplicationName, mApplicationName,
		WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
		posX, posY, screenWidth, screenHeight, NULL, NULL, mHInstance, NULL);

	if (!mHwnd) {
		return false;
	}

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(mHwnd, SW_SHOW);
	SetForegroundWindow(mHwnd);
	SetFocus(mHwnd);

	// Hide the mouse cursor.
	ShowCursor(false);

	return true;
}

//ShutdownWindows returns the screen settings back to normal and releases the window and the handles associated with it.
void System::ShutdownWindows(){
	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if (FULL_SCREEN){
		ChangeDisplaySettings(NULL, 0);
	}

	// Remove the window.
	DestroyWindow(mHwnd);
	mHwnd = NULL;

	// Remove the application instance.
	UnregisterClass(mApplicationName, mHInstance);
	mHInstance = NULL;

	// Release the pointer to this class.
	ApplicationHandle = NULL;

	return;
}

//The WndProc function is where windows sends its messages to.
LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam){
	switch (umessage){
		// Check if the window is being destroyed.
	case WM_DESTROY:{
		PostQuitMessage(0);
		return 0;
	}

	// Check if the window is being closed.
	case WM_CLOSE:{
		PostQuitMessage(0);
		return 0;
	}

	// All other messages pass to the message handler in the system class.
	default:{
		return ApplicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
	}
	}
}