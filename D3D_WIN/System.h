#ifndef _SYSTEM_H_
#define _SYSTEM_H_

//Pre-Processing Directives
#define WIN32_LEAN_AND_MEAN

//Includes
#include <Windows.h>
#include "Input.h"
#include "Game.h"
#include "Time.h"

class System
{
private:
	LPCWSTR mApplicationName;
	HINSTANCE mHInstance;
	HWND mHwnd;

	std::unique_ptr<Input> mInput;
	std::unique_ptr<Game> mGame;
	std::unique_ptr<Time> mTime;

public:
	System();
	System(const System& other);
	~System();
	
	bool Initialize();
	void Shutdown();
	void Run();
	
	LRESULT CALLBACK MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam);

private:
	bool Frame();
	bool InitializeWindows(int& screenWidth, int& screenHeight);
	void ShutdownWindows();
};

//Globals
static System* ApplicationHandle = 0;

//Function Prototypes
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

#endif
