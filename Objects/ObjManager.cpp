#include "ObjManager.h"
#include "eSceneComp.h"
#include "eVehicle.h"
#include "DirectionalLight.h"

ObjManager::ObjManager(){
	mDirectionalLightPresent = false;
}

ObjManager::~ObjManager(){

}

bool ObjManager::Initialize(ID3D11Device* device, HWND hWnd) {
	RECT rect;
	GetClientRect(hWnd, &rect);

	mRenderer.reset(new Renderer);
	if (!mRenderer->Initialize(device, hWnd, rect.right, rect.bottom)) {
		return false;
	}

	mWindowHeight = rect.bottom;
	mWindowWidth = rect.right;

	return true;
}

unsigned int ObjManager::GetNextValidID() {
	if (mObjects.empty()) {
		return 0;
	}
	
	return mObjects.size();
}

eObject* ObjManager::AddObject(eObject* const object) {
	if (!object) {
		return nullptr;
	}

	std::unique_ptr<eObject> obj(object);
	obj->mID = GetNextValidID();

	obj.swap(mObjects[obj->mID]);

	return object;
}

eObject* ObjManager::AddSceneComp(Mesh* const mesh, Texture* const texture, const DirectX::XMFLOAT3 position,
	const DirectX::XMFLOAT3 scale, const DirectX::XMFLOAT3 pitchYawRoll) {
	eSceneComp* sceneComp = new eSceneComp;

	sceneComp->Initialize(mesh, texture, scale, position, pitchYawRoll);

	return AddObject(sceneComp);
}

eObject* ObjManager::AddDirectionalLight(ID3D11Device* device, const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 target,
	const DirectX::XMFLOAT4 diffuseColor, const DirectX::XMFLOAT4 ambientColor, const float intensity) {
	if (mDirectionalLightPresent) {
		return nullptr;
	}
	
	DirectionalLight* directionalLight = new DirectionalLight;

	if (!directionalLight->Initialize(device, position, target, diffuseColor, ambientColor, intensity, mWindowWidth, mWindowHeight)) {
		return false;
	}

	//mDirectionalLightPresent = true;

	return AddObject(directionalLight);
}

bool ObjManager::DeleteAllObjects() {
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator iter = mObjects.begin();

	while (iter != mObjects.end()) {
		iter = mObjects.erase(iter);
	}

	if (!mObjects.empty()) {
		return false;
	}

	return true;
}

bool ObjManager::DeleteObject(unsigned int id) {
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator iter = mObjects.find(id);

	if (iter->second) {
		mObjects.erase(iter);
		return true;
	}

	return false;
}

void ObjManager::UpdateObjects(const float dt) {
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator iter = mObjects.begin();
	
	while (iter != mObjects.end()) {
		if (iter->second) {
			iter->second->Frame(dt);
		}
		iter++;
	}
}

void ObjManager::Render(Direct3D* d3d, const DirectX::XMFLOAT4X4 view) {
	mRenderer->RenderScene(&mObjects, d3d, view);
}