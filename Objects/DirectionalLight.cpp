#include "DirectionalLight.h"
using namespace DirectX;

DirectionalLight::DirectionalLight(){
	mDepthMap = nullptr;
}

DirectionalLight::~DirectionalLight(){
}

bool DirectionalLight::Initialize(ID3D11Device* device, const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 target, const DirectX::XMFLOAT4 diffuseColor, const DirectX::XMFLOAT4 ambientColor, const float intensity, const unsigned int windowWidth, const unsigned int windowHeight) {
	if (!eLightSource::Initialize(position, target, diffuseColor, ambientColor, intensity)) {
		return false;
	}

	mDepthMap.reset(new RenderBuffer);
	if (!mDepthMap->Initialize(device, 4096, 4096)) {
		return false;
	}

	mDownSampleBuffer.reset(new RenderBuffer);
	if (!mDownSampleBuffer->Initialize(device, windowWidth / 4, windowHeight / 4)) {
		return false;
	}

	mLightedSceneBuffer.reset(new RenderBuffer);
	if (!mLightedSceneBuffer->Initialize(device, windowWidth, windowHeight)) {
		return false;
	}

	mBlurBuffer.reset(new RenderBuffer);
	if (!mBlurBuffer->Initialize(device, windowWidth / 4, windowHeight / 4)) {
		return false;
	}

	DirectX::XMStoreFloat3(&mLightDirection, DirectX::XMVector3Normalize(DirectX::XMLoadFloat3(&mPosition) - DirectX::XMLoadFloat3(&mTarget)));

	return true;
}

void DirectionalLight::Frame(const float dt) {
	DirectX::XMStoreFloat4x4(&mTransform, DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&mPosition), DirectX::XMLoadFloat3(&mTarget), DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f)));
}