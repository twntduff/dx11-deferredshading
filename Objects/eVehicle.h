#ifndef _EVEHICLE_H_
#define _EVEHICLE_H_

//Includes
#include "eSceneComp.h"

class eVehicle : public eSceneComp
{
public:
	eVehicle();
	~eVehicle();

	void Frame(const float dt);
};

#endif