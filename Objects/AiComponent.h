#ifndef _AICOMPONENT_H_
#define _AICOMPONENT_H_

//Includes
#include <DirectXMath.h>
#include <memory>
#include "BaseState.h"

class AiComponent
{
public:
	std::unique_ptr<BaseState> mCurrentState;
	std::unique_ptr<BaseState> mPreviousState;

	std::weak_ptr<EngineEntity> mTarget;

	DirectX::XMFLOAT3 mWanderTarget;
	DirectX::XMFLOAT3 mForceAccum;

	float mMaxSpeed;
	float mLookDistance;
	float mPanicDistance;
	float mMaxTurnRate;
	float mWanderDistance;
	float mWanderJitter;

	bool mWander;
	bool mSeek;
	bool mPursue;
	bool mFlee;
	bool mEvade;

public:
	AiComponent();
	~AiComponent();

	bool Initialize(const float maxSpeed, const float mLookDistance);
	void Update(const float dt);
};

#endif