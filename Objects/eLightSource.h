#ifndef _ELIGHTSOURCE_H_
#define _ELIGHTSOURCE_H_

//Includes
#include <memory>
#include "eObject.h"

class eLightSource : public eObject
{
public:
	DirectX::XMFLOAT3 mTarget;
	DirectX::XMFLOAT4 mDiffuseColor;
	DirectX::XMFLOAT4 mAmbientColor;
	float mIntensity;

public:
	eLightSource();
	~eLightSource();

	bool Initialize(const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 target, const DirectX::XMFLOAT4 diffuseColor, const DirectX::XMFLOAT4 ambientColor, const float intensity);
};

#endif