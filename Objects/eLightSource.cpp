#include "eLightSource.h"

eLightSource::eLightSource(){

}

eLightSource::~eLightSource(){

}

bool eLightSource::Initialize(const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 target, const DirectX::XMFLOAT4 diffuseColor, const DirectX::XMFLOAT4 ambientColor, const float intensity) {
	mPosition = position;
	mDiffuseColor = diffuseColor;
	mAmbientColor = ambientColor;
	mTarget = target;
	mIntensity = intensity;

	DirectX::XMStoreFloat4x4(&mTransform, DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&mPosition), DirectX::XMLoadFloat3(&mTarget), DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f)));

	return true;
}