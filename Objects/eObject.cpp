#include "eObject.h"

eObject::eObject(){
	DirectX::XMStoreFloat4x4(&mTransform, DirectX::XMMatrixIdentity());
	mPosition = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	mOrientation = DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
}

eObject::~eObject() {

}

bool eObject::Initialize(const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 pitchYawRoll) {
	mPosition = position;

	DirectX::XMMATRIX rotMatrix = DirectX::XMMatrixRotationRollPitchYawFromVector(XMLoadFloat3(&pitchYawRoll));
	DirectX::XMStoreFloat4(&mOrientation, DirectX::XMQuaternionRotationMatrix(rotMatrix));
	DirectX::XMStoreFloat4x4(&mTransform, rotMatrix * DirectX::XMMatrixTranslationFromVector(XMLoadFloat3(&mPosition)));

	return true;
}

void eObject::Frame(const float dt) {
	DirectX::XMStoreFloat4x4(&mTransform, DirectX::XMMatrixRotationQuaternion(XMLoadFloat4(&mOrientation)) * DirectX::XMMatrixTranslationFromVector(XMLoadFloat3(&mPosition)));
}

DirectX::XMFLOAT3 eObject::GetAxis(const unsigned int axis) {
	return DirectX::XMFLOAT3(mTransform.m[axis][0], mTransform.m[axis][1], mTransform.m[axis][2]);
}