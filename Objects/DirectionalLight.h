#ifndef _DIRECTIONALLIGHT_H_
#define _DIRECTIONALLIGHT_H_

//Includes
#include <DirectXMath.h>
#include "eLightSource.h"
#include "RenderBuffer.h"

class DirectionalLight : public eLightSource
{
public:
	DirectX::XMFLOAT3 mLightDirection;
	std::unique_ptr<RenderBuffer> mDepthMap;
	std::unique_ptr<RenderBuffer> mLightedSceneBuffer;
	std::unique_ptr<RenderBuffer> mDownSampleBuffer;
	std::unique_ptr<RenderBuffer> mBlurBuffer;

public:
	DirectionalLight();
	~DirectionalLight();

	bool Initialize(ID3D11Device* device, const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 target, const DirectX::XMFLOAT4 diffuseColor, const DirectX::XMFLOAT4 ambientColor, const float intensity, const unsigned int windowWidth, const unsigned int windowHeight);
	void Frame(const float dt);
};

#endif