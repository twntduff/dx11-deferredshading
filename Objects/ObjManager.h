#ifndef _OBJMANAGER_H_
#define _OBJMANAGER_H_

//Includes
#include "Renderer.h"
#include "Texture.h"
#include "Mesh.h"

//Globals
const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = false;
const float SCREEN_DEPTH = 2000.0f;
const float SCREEN_NEAR = 0.01f;
const unsigned int SCREEN_WIDTH = 900;
const unsigned int SCREEN_HEIGHT = 600;

class ObjManager
{
private:
	unsigned int mWindowWidth;
	unsigned int mWindowHeight;
	bool mDirectionalLightPresent;
	std::unique_ptr<Renderer> mRenderer;
	std::unordered_map<unsigned int, std::unique_ptr<eObject>> mObjects;

public:
	ObjManager();
	~ObjManager();

	bool Initialize(ID3D11Device* device, HWND hWnd);
	unsigned int GetNextValidID();
	eObject* AddObject(eObject* const object);
	eObject* AddSceneComp(Mesh* const mesh, Texture* const texture, const DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f),
		const DirectX::XMFLOAT3 scale = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f), const DirectX::XMFLOAT3 pitchYawRoll = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	eObject* AddDirectionalLight(ID3D11Device* device, const DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(1000.0f, 1000.0f, 0.0f), const DirectX::XMFLOAT3 target = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f),
		const DirectX::XMFLOAT4 diffuseColor = DirectX::XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), const DirectX::XMFLOAT4 ambientColor = DirectX::XMFLOAT4(0.15f, 0.15f, 0.15f, 1.0f), const float intensity = 0.5f);
	bool DeleteAllObjects();
	bool DeleteObject(unsigned int id);
	void UpdateObjects(const float dt);
	void Render(Direct3D* d3d, const DirectX::XMFLOAT4X4 view);
};

#endif