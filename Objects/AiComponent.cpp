#include "AiComponent.h"

AiComponent::AiComponent(){
	mCurrentState.reset(new PatrolState);
	mPreviousState.reset(new PatrolState);

	mTarget.reset();

	mMaxSpeed = 0.0f;
	mLookDistance = 0.0f;

	mWander = false;
	mSeek = false;
	mPursue = false;
	mFlee = false;
	mEvade = false;
}

AiComponent::~AiComponent(){

}

bool AiComponent::Initialize(const float maxSpeed, const float lookDistance) {
	mMaxSpeed = maxSpeed;
	mLookDistance = lookDistance;

	return true;
}

void AiComponent::Update(const float dt) {
	
}