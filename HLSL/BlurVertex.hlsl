cbuffer BlurVBuffer
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
    float screenWidth;
    float screenHeight;
    float2 padding;
};

struct VertexInputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;

    float2 texXCoord1 : TEXCOORD1;
    float2 texXCoord2 : TEXCOORD2;
    float2 texXCoord3 : TEXCOORD3;
    float2 texXCoord4 : TEXCOORD4;
    float2 texXCoord5 : TEXCOORD5;

    float2 texYCoord1 : TEXCOORD10;
    float2 texYCoord2 : TEXCOORD11;
    float2 texYCoord3 : TEXCOORD12;
    float2 texYCoord4 : TEXCOORD13;
    float2 texYCoord5 : TEXCOORD14;
};

PixelInputType VS_MAIN(VertexInputType input)
{
    PixelInputType output;
    float texelXSize;
    float texelYSize;

    input.position.w = 1.0f;
    output.position = mul(input.position, worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);

    output.tex = input.tex;

    texelXSize = 1.0f / screenWidth;
    texelYSize = 1.0f / screenHeight;

    output.texXCoord1 = input.tex + float2(texelXSize * -8.0f, 0.0f);
    output.texXCoord2 = input.tex + float2(texelXSize * -6.0f, 0.0f);
    output.texXCoord3 = input.tex + float2(texelXSize * -4.0f, 0.0f);
    output.texXCoord4 = input.tex + float2(texelXSize * -2.0f, 0.0f);
    output.texXCoord5 = input.tex + float2(texelXSize * 0.0f, 0.0f);

    output.texYCoord1 = input.tex + float2(0.0f, texelYSize * -8.0f);
    output.texYCoord2 = input.tex + float2(0.0f, texelYSize * -6.0f);
    output.texYCoord3 = input.tex + float2(0.0f, texelYSize * -4.0f);
    output.texYCoord4 = input.tex + float2(0.0f, texelYSize * -2.0f);
    output.texYCoord5 = input.tex + float2(0.0f, texelYSize * 0.0f);

	return output;
}