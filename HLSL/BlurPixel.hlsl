Texture2D blackWhiteTexture;
SamplerState SampleTypePoint;

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;

    float2 texXCoord1 : TEXCOORD1;
    float2 texXCoord2 : TEXCOORD2;
    float2 texXCoord3 : TEXCOORD3;
    float2 texXCoord4 : TEXCOORD4;
    float2 texXCoord5 : TEXCOORD5;

    float2 texYCoord1 : TEXCOORD10;
    float2 texYCoord2 : TEXCOORD11;
    float2 texYCoord3 : TEXCOORD12;
    float2 texYCoord4 : TEXCOORD13;
    float2 texYCoord5 : TEXCOORD14;
};

float4 PS_MAIN(PixelInputType input) : SV_TARGET
{
    float weight0, weight1, weight2, weight3, weight4;
    float normalization;
    float4 color;

    weight0 = 1.0f;
    weight1 = 0.9f;
    weight2 = 0.55f;
    weight3 = 0.18f;
    weight4 = 0.1f;

    normalization = (weight0 + 2.0f * (weight1 + weight2 + weight3 + weight4));

    weight0 = weight0 / normalization;
    weight1 = weight1 / normalization;
    weight2 = weight2 / normalization;
    weight3 = weight3 / normalization;
    weight4 = weight4 / normalization;

    color = float4(0.0f, 0.0f, 0.0f, 1.0f);

    color += blackWhiteTexture.Sample(SampleTypePoint, input.texXCoord1) * weight4;
    color += blackWhiteTexture.Sample(SampleTypePoint, input.texXCoord2) * weight3;
    color += blackWhiteTexture.Sample(SampleTypePoint, input.texXCoord3) * weight2;
    color += blackWhiteTexture.Sample(SampleTypePoint, input.texXCoord4) * weight1;
    color += blackWhiteTexture.Sample(SampleTypePoint, input.texXCoord5) * weight0;

    color += blackWhiteTexture.Sample(SampleTypePoint, input.texYCoord1) * weight4;
    color += blackWhiteTexture.Sample(SampleTypePoint, input.texYCoord2) * weight3;
    color += blackWhiteTexture.Sample(SampleTypePoint, input.texYCoord3) * weight2;
    color += blackWhiteTexture.Sample(SampleTypePoint, input.texYCoord4) * weight1;
    color += blackWhiteTexture.Sample(SampleTypePoint, input.texYCoord5) * weight0;

    color = saturate(color);
    color.w = 1.0f;

	return color;
}