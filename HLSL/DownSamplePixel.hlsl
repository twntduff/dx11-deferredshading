Texture2D DepthMap;
SamplerState SampleTypeClamp;

cbuffer LightBuffer
{
    float4 AmbientColor;
    float4 DiffuseColor;
    float3 LightDirection;
    float Intensity;
}

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD;
    float3 normal : NORMAL0;
    float4 lightViewPosition : NORMAL1;
};

float4 PS_MAIN(PixelInputType input) : SV_TARGET
{
    float bias = 0.005f;
    float4 color;
    float2 projectTexCoord;
    float depthValue;
    float lightDepthValue;
    float lightIntensity;

    color = AmbientColor;
    color = saturate(color);

    projectTexCoord.x = input.lightViewPosition.x / input.lightViewPosition.w / 2.0f + 0.5f;
    projectTexCoord.y = input.lightViewPosition.y / input.lightViewPosition.w / -2.0f + 0.5f;

    if ((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
    {
        lightDepthValue = (input.lightViewPosition.z * 10.0f) - bias;
        depthValue = DepthMap.Sample(SampleTypeClamp, projectTexCoord).r;

        if (lightDepthValue < depthValue)
        {
            lightIntensity = saturate(dot(LightDirection, input.normal));

            if (lightIntensity > 0.0f)
            {
                color += (DiffuseColor * lightIntensity);
                color = saturate(color);
            }
        }
    }
    else
    {
        lightIntensity = saturate(dot(input.normal, LightDirection));

        if (lightIntensity > 0.0f)
        {
            color += (DiffuseColor * lightIntensity);
            color = saturate(color);
        }
    }
    
    return color * Intensity;
}