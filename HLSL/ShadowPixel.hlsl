struct PixelInputType
{
	float4 position : SV_POSITION;
	float4 depthPosition : TEXTURE0;
};

float4 PS_MAIN(PixelInputType input) : SV_TARGET
{
	float depthValue;

    depthValue = input.depthPosition.z * 10.0f;

	return float4(depthValue, depthValue, depthValue, 1.0f);
}