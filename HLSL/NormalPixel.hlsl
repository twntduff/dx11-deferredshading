struct PixelInputType
{
	float4 position : SV_POSITION;
	float3 normal : NORMAL0;
};

float4 PS_MAIN(PixelInputType input) : SV_TARGET
{
	return float4(input.normal, 1.0f);
}