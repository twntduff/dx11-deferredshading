cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix lightViewMatrix;
	matrix lightProjMatrix;
};

struct VertexInputType
{
	float4 position : POSITION;
};

struct PixelInputType
{
	float4 position : SV_POSITION;
	float4 depthPosition : TEXTURE0;
};

PixelInputType VS_MAIN(VertexInputType input)
{
	PixelInputType output;

	input.position.w = 1.0f;
	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, lightViewMatrix);
	output.position = mul(output.position, lightProjMatrix);

    output.depthPosition = output.position;

	return output;
}