Texture2D shaderTexture;
SamplerState SampleTypeWrap;

struct PixelInputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD;
};

float4 PS_MAIN(PixelInputType input) : SV_TARGET
{
	return shaderTexture.Sample(SampleTypeWrap, input.tex);
}