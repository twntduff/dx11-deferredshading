Texture2DArray DownSamples : register(t0);
Texture2D TextureMap : register(t1);
Texture2D NormalMap : register(t2);
SamplerState SampleTypePoint;

cbuffer LightBuffer
{
    int NumLights;
    float3 Padding;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
};

float4 PS_MAIN(PixelInputType input) : SV_TARGET
{
    float4 texColor;
    float4 normals;
    float4 depthValue = float4(0.0f, 0.0f, 0.0f, 1.0f);
    float4 outputColor;

    texColor = TextureMap.Sample(SampleTypePoint, input.tex);
    normals = NormalMap.Sample(SampleTypePoint, input.tex);

    for (int i = 0; i < NumLights; i++)
    {
        depthValue += DownSamples.Sample(SampleTypePoint, float3(input.tex.x, input.tex.y, i));
        depthValue = saturate(depthValue);
    }

    outputColor = saturate(texColor * depthValue);
    
    return outputColor;
}