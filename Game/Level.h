#ifndef _LEVEL_H_
#define _LEVEL_H_

//Includes
#include "MeshManager.h"
#include "TextureManager.h"
#include "Camera.h"
#include "DirectionalLight.h"
#include "ObjManager.h"
#include "Direct3D.h"

class Level
{
public:
	std::unique_ptr<ObjManager> mObjectManager;
	std::unique_ptr<MeshManager> mMeshManager;
	std::unique_ptr<TextureManager> mTextureManager;
	Camera* mCamera;

public:
	Level();
	~Level();

	bool Initialize(ID3D11Device* device, HWND hWnd, Input* input);
	void Frame(const float dt);
	void Render(Direct3D* d3d);
	bool Exit();
};

#endif