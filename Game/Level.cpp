#include "Level.h"

Level::Level(){

}

Level::~Level(){

}

bool Level::Initialize(ID3D11Device* device, HWND hWnd, Input* input) {
	mMeshManager.reset(new MeshManager);
	mTextureManager.reset(new TextureManager);

	mObjectManager.reset(new ObjManager);
	if (!mObjectManager->Initialize(device, hWnd)) {
		return false;
	}

	// Create the camera object.
	mCamera = new Camera;
	if (!mCamera->Initialize(input, DirectX::XMFLOAT3(0.0f, 0.0f, -10.0f))) {
		return false;
	}

	mObjectManager->AddObject(mCamera);

	return true;
}

void Level::Frame(const float dt) {
	mObjectManager->UpdateObjects(dt);
}

void Level::Render(Direct3D* d3d) {
	mObjectManager->Render(d3d, mCamera->mTransform);
}

bool Level::Exit() {
	mMeshManager.release();
	mTextureManager.release();

	return true;
}