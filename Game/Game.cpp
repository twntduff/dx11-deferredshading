#include "Game.h"
#include <dxgidebug.h>

Game::Game() {
	mD3D = nullptr;
	mCurrentLevel = nullptr;
}

Game::~Game() {
	//Shutdown();
}

bool Game::Initialize(int screenWidth, int screenHeight, HWND hwnd) {
	bool result;

	// Create the Direct3D object.
	mD3D.reset(new Direct3D);
	result = mD3D->Initialize(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR);
	if (!result) {
		MessageBox(hwnd, L"Could not initialize Direct3D", L"Error", MB_OK);
		return false;
	}

	return true;
}

bool Game::Frame(float dt) {
	//Update level objects
	mCurrentLevel->Frame(dt);

	// Render the scene.
	if(!Render()){
		return false;
	}

	return true;
}

bool Game::Render() {
	// Clear the buffers to begin the scene.
	mD3D->BeginScene(0.5f, 0.5f, 0.5f, 1.0f);

	mCurrentLevel->Render(mD3D.get());
	
	// Present the rendered scene to the screen.
	mD3D->EndScene();

	return true;
}

void Game::LoadTestLevel(Input* input, HWND hWnd) {
	mCurrentLevel.reset(new Level);
	if (!mCurrentLevel->Initialize(mD3D->mDevice, hWnd, input)) {
		return;
	}

	mCurrentLevel->mMeshManager->LoadAndRegisterMeshFromOBJ(mD3D->mDevice, "3D_Models\\Carpet.obj");
	mCurrentLevel->mTextureManager->LoadAndRegisterTexture(mD3D->mDevice, "Textures\\Carpet.png");
	mCurrentLevel->mMeshManager->LoadAndRegisterMeshFromOBJ(mD3D->mDevice, "3D_Models\\Box.obj");
	mCurrentLevel->mTextureManager->LoadAndRegisterTexture(mD3D->mDevice, "Textures\\Seamless_Stone.png");
	mCurrentLevel->mTextureManager->LoadAndRegisterTexture(mD3D->mDevice, "Textures\\Seamless_Crate.png");
	mCurrentLevel->mTextureManager->LoadAndRegisterTexture(mD3D->mDevice, "Textures\\Seamless_Marble.png");

	mCurrentLevel->mObjectManager->AddDirectionalLight(mD3D->mDevice, DirectX::XMFLOAT3(-200.0f, 200.0f, 0.0f), DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	mCurrentLevel->mObjectManager->AddDirectionalLight(mD3D->mDevice, DirectX::XMFLOAT3(-200.0f, 200.0f, 100.0f), DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));

	mCurrentLevel->mObjectManager->AddSceneComp(mCurrentLevel->mMeshManager->RetrieveMesh("Carpet"), mCurrentLevel->mTextureManager->RetrieveTexture("Seamless_Marble"), DirectX::XMFLOAT3(0.0f, -1.1f, 0.0f));
	mCurrentLevel->mObjectManager->AddSceneComp(mCurrentLevel->mMeshManager->RetrieveMesh("Box"), mCurrentLevel->mTextureManager->RetrieveTexture("Seamless_Crate"), DirectX::XMFLOAT3(-2.0f, 0.0f, -4.0f));
	mCurrentLevel->mObjectManager->AddSceneComp(mCurrentLevel->mMeshManager->RetrieveMesh("Box"), mCurrentLevel->mTextureManager->RetrieveTexture("Seamless_Crate"), DirectX::XMFLOAT3(2.0f, 0.0f, 2.0f));
	mCurrentLevel->mObjectManager->AddSceneComp(mCurrentLevel->mMeshManager->RetrieveMesh("Box"), mCurrentLevel->mTextureManager->RetrieveTexture("Seamless_Stone"), DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f));
}