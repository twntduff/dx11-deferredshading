#pragma once

//Includes
#include <memory>
#include "Direct3D.h"
#include "Level.h"

class Game
{
private:
	std::unique_ptr<Direct3D> mD3D;
	std::unique_ptr<Level> mCurrentLevel;

public:
	Game();
	~Game();

	bool Initialize(int screenWidth, int screenHeight, HWND hwnd);
	bool Frame(float dt);
	bool Render();

	//DEBUG
	void LoadTestLevel(Input* input, HWND hWnd);
};

