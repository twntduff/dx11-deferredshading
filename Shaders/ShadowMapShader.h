#ifndef _SHADOWMAPSHADER_H_
#define _SHADOWMAPSHADER_H_

//Includes
#include <d3d11.h>
#include <atlbase.h>
#include <d3dcompiler.h>
#include <string>
#include <DirectXMath.h>

struct ShadowBuffer {
	DirectX::XMFLOAT4X4 World;
	DirectX::XMFLOAT4X4 LightView;
	DirectX::XMFLOAT4X4 LightProjection;
};

class ShadowMapShader
{
public:
	ATL::CComPtr<ID3D11VertexShader> mVertexShader;
	ATL::CComPtr<ID3D11PixelShader> mPixelShader;
	ATL::CComPtr<ID3D11InputLayout> mInputLayout;
	ATL::CComPtr<ID3D11Buffer> mMatrixBuffer;

public:
	ShadowMapShader();
	~ShadowMapShader();

	bool Initialize(ID3D11Device* device, HWND hWnd);
	void Render(ID3D11DeviceContext* deviceContext, const unsigned int indexCount, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 lightView, const DirectX::XMFLOAT4X4 lightProjection);
};

#endif