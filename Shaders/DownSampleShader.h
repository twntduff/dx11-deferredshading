#ifndef DOWNSAMPLESHADER_H_
#define DOWNSAMPLESHADER_H_

//Includes
#include <d3d11.h>
#include <atlbase.h>
#include <d3dcompiler.h>
#include <string>
#include <memory>
#include <DirectXMath.h>
#include "RenderBuffer.h"

struct DownSampleMatrixBuffer {
	DirectX::XMFLOAT4X4 World;
	DirectX::XMFLOAT4X4 View;
	DirectX::XMFLOAT4X4 Projection;
	DirectX::XMFLOAT4X4 LightView;
};

struct DownSampleLightBuffer {
	DirectX::XMFLOAT4 AmbientColor;
	DirectX::XMFLOAT4 DiffuseColor;
	DirectX::XMFLOAT3 LightDirection;
	float Intensity;
};

class DownSampleShader
{
private:
	ATL::CComPtr<ID3D11VertexShader> mVertexShader;
	ATL::CComPtr<ID3D11PixelShader> mPixelShader;
	ATL::CComPtr<ID3D11InputLayout> mInputLayout;
	ATL::CComPtr<ID3D11Buffer> mMatrixBuffer;
	ATL::CComPtr<ID3D11Buffer> mLightBuffer;
	ATL::CComPtr<ID3D11SamplerState> mSampleStateClamp;

public:
	DownSampleShader();
	~DownSampleShader();

	bool Initialize(ID3D11Device* device, HWND hWnd, const unsigned int windowWidth, const unsigned int windowHeight);
	void Render(ID3D11DeviceContext* deviceContext, const unsigned int indexCount, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection, const DirectX::XMFLOAT4X4 lightView, const DirectX::XMFLOAT4 ambient, const DirectX::XMFLOAT4 diffuse, const float intensity, const DirectX::XMFLOAT3 lightDirection, ID3D11ShaderResourceView* depthMap);
};

#endif