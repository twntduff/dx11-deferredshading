#include "DownSampleShader.h"

DownSampleShader::DownSampleShader(){

}

DownSampleShader::~DownSampleShader(){

}

bool DownSampleShader::Initialize(ID3D11Device* device, HWND hWnd, const unsigned int windowWidth, const unsigned int windowHeight) {
	ID3DBlob *errorMessage;
	ID3DBlob *vertexShaderBuffer;
	ID3DBlob *pixelShaderBuffer;
	D3D11_SAMPLER_DESC samplerDesc;
	HRESULT hresult;

	hresult = D3DCompileFromFile(L"DownSampleVertex.hlsl", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS_MAIN", "vs_5_0", D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG, 0, &vertexShaderBuffer, &errorMessage);
	if (FAILED(hresult)) {
		return false;
	}
	hresult = D3DCompileFromFile(L"DownSamplePixel.hlsl", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PS_MAIN", "ps_5_0", D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG, 0, &pixelShaderBuffer, &errorMessage);
	if (FAILED(hresult)) {
		return false;
	}
	hresult = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), NULL, &mVertexShader);
	if (FAILED(hresult)) {
		return false;
	}
	hresult = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), NULL, &mPixelShader);
	if (FAILED(hresult)) {
		return false;
	}

	D3D11_INPUT_ELEMENT_DESC polygonLayout[3];
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	polygonLayout[2].SemanticName = "NORMAL";
	polygonLayout[2].SemanticIndex = 0;
	polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[2].InputSlot = 0;
	polygonLayout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[2].InstanceDataStepRate = 0;

	unsigned int numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	hresult = device->CreateInputLayout(polygonLayout, numElements, vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), &mInputLayout);
	if (FAILED(hresult)) {
		return false;
	}

	vertexShaderBuffer->Release();
	vertexShaderBuffer = 0;

	pixelShaderBuffer->Release();
	pixelShaderBuffer = 0;

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	hresult = device->CreateSamplerState(&samplerDesc, &mSampleStateClamp);
	if (FAILED(hresult))
	{
		return false;
	}

	D3D11_BUFFER_DESC matrixBufferDesc;
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(DownSampleMatrixBuffer);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	hresult = device->CreateBuffer(&matrixBufferDesc, NULL, &mMatrixBuffer);
	if (FAILED(hresult)) {
		return false;
	}

	D3D11_BUFFER_DESC lightBufferDesc;
	lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc.ByteWidth = sizeof(DownSampleLightBuffer);
	lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc.MiscFlags = 0;
	lightBufferDesc.StructureByteStride = 0;

	hresult = device->CreateBuffer(&lightBufferDesc, NULL, &mLightBuffer);
	if (FAILED(hresult)) {
		return false;
	}

	return true;
}

void DownSampleShader::Render(ID3D11DeviceContext* deviceContext, const unsigned int indexCount, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection, const DirectX::XMFLOAT4X4 lightView, const DirectX::XMFLOAT4 ambient, const DirectX::XMFLOAT4 diffuse, const float intensity, const DirectX::XMFLOAT3 lightDirection, ID3D11ShaderResourceView* depthMap) {
	HRESULT hresult;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	DownSampleMatrixBuffer* matrixDataPtr;
	DownSampleLightBuffer* lightDataPtr;

	//Set shader parameters
	DirectX::XMFLOAT4X4 xmWorld;
	DirectX::XMFLOAT4X4 xmView;
	DirectX::XMFLOAT4X4 xmProjection;
	DirectX::XMFLOAT4X4 xmLightView;

	DirectX::XMStoreFloat4x4(&xmWorld, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&world)));
	DirectX::XMStoreFloat4x4(&xmView, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&view)));
	DirectX::XMStoreFloat4x4(&xmProjection, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&projection)));
	DirectX::XMStoreFloat4x4(&xmLightView, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&lightView)));

	hresult = deviceContext->Map(mMatrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(hresult)) {
		return;
	}

	matrixDataPtr = (DownSampleMatrixBuffer*)mappedResource.pData;
	matrixDataPtr->World = xmWorld;
	matrixDataPtr->View = xmView;
	matrixDataPtr->Projection = xmProjection;
	matrixDataPtr->LightView = xmLightView;

	deviceContext->Unmap(mMatrixBuffer, 0);

	deviceContext->VSSetConstantBuffers(0, 1, &mMatrixBuffer.p);

	hresult = deviceContext->Map(mLightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(hresult)) {
		return;
	}

	lightDataPtr = (DownSampleLightBuffer*)mappedResource.pData;
	lightDataPtr->AmbientColor = ambient;
	lightDataPtr->DiffuseColor = diffuse;
	lightDataPtr->LightDirection = lightDirection;
	lightDataPtr->Intensity = intensity;

	deviceContext->Unmap(mLightBuffer, 0);

	deviceContext->PSSetConstantBuffers(0, 1, &mLightBuffer.p);

	deviceContext->PSSetShaderResources(0, 1, &depthMap);

	//Render shader
	deviceContext->IASetInputLayout(mInputLayout);

	deviceContext->VSSetShader(mVertexShader, NULL, 0);
	deviceContext->PSSetShader(mPixelShader, NULL, 0);

	deviceContext->PSSetSamplers(0, 1, &mSampleStateClamp.p);

	deviceContext->DrawIndexed(indexCount, 0, 0);
}