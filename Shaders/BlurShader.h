#ifndef _BLURSHADER_H_
#define _BLURSHADER_H_

//Includes
#include <d3d11.h>
#include <atlbase.h>
#include <d3dcompiler.h>
#include <string>
#include <memory>
#include <DirectXMath.h>
#include "RenderBuffer.h"

struct BlurBuffer {
	DirectX::XMFLOAT4X4 World;
	DirectX::XMFLOAT4X4 View;
	DirectX::XMFLOAT4X4 Projection;
	float ScreenWidth;
	float ScreenHeight;
	DirectX::XMFLOAT2 Padding;
};

class BlurShader
{
private:
	ATL::CComPtr<ID3D11VertexShader> mVertexShader;
	ATL::CComPtr<ID3D11PixelShader> mPixelShader;
	ATL::CComPtr<ID3D11InputLayout> mInputLayout;
	ATL::CComPtr<ID3D11Buffer> mBlurBuffer;
	ATL::CComPtr<ID3D11SamplerState> mSampleStateWrap;

public:
	std::unique_ptr<RenderBuffer> mBlurRenderBuffer;

public:
	BlurShader();
	~BlurShader();

	bool Initialize(ID3D11Device* device, HWND hWnd, const unsigned int windowWidth, const unsigned int windowHeight);
	void Render(ID3D11DeviceContext* deviceContext, const unsigned int indexCount, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection, const float screenWidth, const float screenHeight, ID3D11ShaderResourceView* blackWhiteMap);
};

#endif