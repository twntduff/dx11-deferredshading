#ifndef _NORMALMAPSHADER_H_
#define _NORMALMAPSHADER_H_

//Includes
#include <d3d11.h>
#include <atlbase.h>
#include <d3dcompiler.h>
#include <string>
#include <memory>
#include <DirectXMath.h>
#include "RenderBuffer.h"

struct NormalMatrixBuffer {
	DirectX::XMFLOAT4X4 World;
	DirectX::XMFLOAT4X4 View;
	DirectX::XMFLOAT4X4 Projection;
};

class NormalMapShader
{
private:
	ATL::CComPtr<ID3D11VertexShader> mVertexShader;
	ATL::CComPtr<ID3D11PixelShader> mPixelShader;
	ATL::CComPtr<ID3D11InputLayout> mInputLayout;
	ATL::CComPtr<ID3D11Buffer> mMatrixBuffer;

public:
	std::unique_ptr<RenderBuffer> mNormalBuffer;

public:
	NormalMapShader();
	~NormalMapShader();

	bool Initialize(ID3D11Device* device, HWND hWnd, const unsigned int windowWidth, const unsigned int windowHeight);
	void Render(ID3D11DeviceContext* deviceContext, const unsigned int indexCount, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection);
};

#endif