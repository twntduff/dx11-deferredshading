#include "BlurShader.h"

BlurShader::BlurShader(){

}

BlurShader::~BlurShader(){

}

bool BlurShader::Initialize(ID3D11Device* device, HWND hWnd, const unsigned int windowWidth, const unsigned int windowHeight) {
	ID3DBlob *errorMessage;
	ID3DBlob *vertexShaderBuffer;
	ID3DBlob *pixelShaderBuffer;
	D3D11_SAMPLER_DESC samplerDesc;
	HRESULT hresult;

	hresult = D3DCompileFromFile(L"BlurVertex.hlsl", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS_MAIN", "vs_5_0", D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG, 0, &vertexShaderBuffer, &errorMessage);
	if (FAILED(hresult)) {
		return false;
	}
	hresult = D3DCompileFromFile(L"BlurPixel.hlsl", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PS_MAIN", "ps_5_0", D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG, 0, &pixelShaderBuffer, &errorMessage);
	if (FAILED(hresult)) {
		return false;
	}
	hresult = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), NULL, &mVertexShader);
	if (FAILED(hresult)) {
		return false;
	}
	hresult = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), NULL, &mPixelShader);
	if (FAILED(hresult)) {
		return false;
	}

	D3D11_INPUT_ELEMENT_DESC polygonLayout[2];
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	unsigned int numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	hresult = device->CreateInputLayout(polygonLayout, numElements, vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), &mInputLayout);
	if (FAILED(hresult)) {
		return false;
	}

	vertexShaderBuffer->Release();
	vertexShaderBuffer = 0;

	pixelShaderBuffer->Release();
	pixelShaderBuffer = 0;

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	hresult = device->CreateSamplerState(&samplerDesc, &mSampleStateWrap);
	if (FAILED(hresult))
	{
		return false;
	}

	D3D11_BUFFER_DESC matrixBufferDesc;
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(BlurBuffer);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	hresult = device->CreateBuffer(&matrixBufferDesc, NULL, &mBlurBuffer);
	if (FAILED(hresult)) {
		return false;
	}

	mBlurRenderBuffer.reset(new RenderBuffer);
	if (!mBlurRenderBuffer->Initialize(device, windowWidth, windowHeight)) {
		return false;
	}

	return true;
}

void BlurShader::Render(ID3D11DeviceContext* deviceContext, const unsigned int indexCount, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection, const float screenWidth, const float screenHeight, ID3D11ShaderResourceView* blackWhiteMap) {
	HRESULT hresult;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	BlurBuffer* dataPtr;

	//Set shader parameters
	DirectX::XMFLOAT4X4 xmWorld;
	DirectX::XMFLOAT4X4 xmView;
	DirectX::XMFLOAT4X4 xmProjection;

	DirectX::XMStoreFloat4x4(&xmWorld, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&world)));
	DirectX::XMStoreFloat4x4(&xmView, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&view)));
	DirectX::XMStoreFloat4x4(&xmProjection, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&projection)));

	hresult = deviceContext->Map(mBlurBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(hresult)) {
		return;
	}

	dataPtr = (BlurBuffer*)mappedResource.pData;
	dataPtr->World = xmWorld;
	dataPtr->View = xmView;
	dataPtr->Projection = xmProjection;
	dataPtr->ScreenWidth = screenWidth;
	dataPtr->ScreenHeight = screenHeight;
	dataPtr->Padding = DirectX::XMFLOAT2(0.0f, 0.0f);

	deviceContext->Unmap(mBlurBuffer, 0);

	deviceContext->VSSetConstantBuffers(0, 1, &mBlurBuffer.p);

	deviceContext->PSSetShaderResources(0, 1, &blackWhiteMap);

	//Render shader
	deviceContext->IASetInputLayout(mInputLayout);

	deviceContext->VSSetShader(mVertexShader, NULL, 0);
	deviceContext->PSSetShader(mPixelShader, NULL, 0);

	deviceContext->DrawIndexed(indexCount, 0, 0);
}