#ifndef _SHADOWPOSTPROCESSING_H_
#define _SHADOWPOSTPROCESSING_H_

//Includes
#include <vector>
#include <d3d11.h>
#include <atlbase.h>
#include <d3dcompiler.h>
#include <string>
#include <DirectXMath.h>

struct PostMatrixBuffer {
	DirectX::XMFLOAT4X4 World;
	DirectX::XMFLOAT4X4 View;
	DirectX::XMFLOAT4X4 Orthographic;
};

struct PostLightBuffer {
	int NumLights;
	DirectX::XMFLOAT3 Padding;
};

class ShadowPostProcessing
{
public:
	ATL::CComPtr<ID3D11VertexShader> mVertexShader;
	ATL::CComPtr<ID3D11PixelShader> mPixelShader;
	ATL::CComPtr<ID3D11InputLayout> mInputLayout;
	ATL::CComPtr<ID3D11Buffer> mMatrixBuffer;
	ATL::CComPtr<ID3D11Buffer> mLightBuffer;
	ATL::CComPtr<ID3D11SamplerState> mSampleStatePoint;

public:
	ShadowPostProcessing();
	~ShadowPostProcessing();

	bool Initialize(ID3D11Device* device, HWND hWnd);
	void Render(ID3D11DeviceContext* deviceContext, const unsigned int indexCount, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 orthographic, unsigned int numLights, ID3D11ShaderResourceView** downSamples, ID3D11ShaderResourceView* textureMap, ID3D11ShaderResourceView* normalMap);
};

#endif