#ifndef _RENDERER_H
#define _RENDERER_H_

//Includes
#include <unordered_map>
#include <stdio.h>
#include <stdarg.h>
#include "Direct3D.h"
#include "eObject.h"
#include "ShadowMapShader.h"
#include "TextureMapShader.h"
#include "NormalMapShader.h"
#include "ShadowPostProcessing.h"
#include "OrthographicWindow.h"
#include "BlurShader.h"
#include "DownSampleShader.h"

class Renderer
{
public:
	unsigned int mWindowWidth;
	unsigned int mWindowHeight;
	
	std::unique_ptr<OrthographicWindow> mFullWindow;
	std::unique_ptr<OrthographicWindow> mSmallWindow;
	std::unique_ptr<DownSampleShader> mDownSampleShader;
	std::unique_ptr<TextureMapShader> mTextureMapShader;
	std::unique_ptr<NormalMapShader> mNormalMapShader;
	std::unique_ptr<ShadowMapShader> mShadowMapShader;
	std::unique_ptr<BlurShader> mBlurShader;
	std::unique_ptr<ShadowPostProcessing> mShadowPostShader;

public:
	Renderer();
	~Renderer();

	bool Initialize(ID3D11Device* device, HWND hWnd, unsigned int windowWidth, unsigned int windowHeight);

	void RenderScene(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, Direct3D* d3d, const DirectX::XMFLOAT4X4 view);
	void RenderPostProcesses(ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 orthographic);

	void RenderDepthMaps(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection);
	void RenderTextureMap(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection);
	void RenderNormalMap(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection);
	void RenderLightedScene(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection);
	void RenderShadowBlurMap(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 orthographic);

	void RenderShadowPostProcess(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11Device* device, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 orthographic);
};

#endif