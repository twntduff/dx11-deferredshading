#include "OrthographicWindow.h"

OrthographicWindow::OrthographicWindow(){

}

OrthographicWindow::~OrthographicWindow(){

}

bool OrthographicWindow::Initialize(ID3D11Device* device, unsigned int windowWidth, unsigned int windowHeight) {
	float left, right, top, bottom;
	WindowWertex* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	int i;

	// Calculate the screen coordinates of the left side of the window.
	left = (float)((windowWidth / 2.0f) * -1.0f);

	// Calculate the screen coordinates of the right side of the window.
	right = left + (float)windowWidth;

	// Calculate the screen coordinates of the top of the window.
	top = (float)(windowHeight / 2.0f);

	// Calculate the screen coordinates of the bottom of the window.
	bottom = top - (float)windowHeight;

	// Set the number of vertices in the vertex array.
	mVertexCount = 6;

	// Set the number of indices in the index array.
	mIndexCount = mVertexCount;

	// Create the vertex array.
	vertices = new WindowWertex[mVertexCount];
	if (!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[mIndexCount];
	if (!indices)
	{
		return false;
	}

	// Load the vertex array with data.
	// First triangle.
	vertices[0].Position = DirectX::XMFLOAT3(left, top, 0.0f);  // Top left.
	vertices[0].Texture = DirectX::XMFLOAT2(0.0f, 0.0f);

	vertices[1].Position = DirectX::XMFLOAT3(right, bottom, 0.0f);  // Bottom right.
	vertices[1].Texture = DirectX::XMFLOAT2(1.0f, 1.0f);

	vertices[2].Position = DirectX::XMFLOAT3(left, bottom, 0.0f);  // Bottom left.
	vertices[2].Texture = DirectX::XMFLOAT2(0.0f, 1.0f);

	// Second triangle.
	vertices[3].Position = DirectX::XMFLOAT3(left, top, 0.0f);  // Top left.
	vertices[3].Texture = DirectX::XMFLOAT2(0.0f, 0.0f);

	vertices[4].Position = DirectX::XMFLOAT3(right, top, 0.0f);  // Top right.
	vertices[4].Texture = DirectX::XMFLOAT2(1.0f, 0.0f);

	vertices[5].Position = DirectX::XMFLOAT3(right, bottom, 0.0f);  // Bottom right.
	vertices[5].Texture = DirectX::XMFLOAT2(1.0f, 1.0f);

	// Load the index array with data.
	for (i = 0; i<mIndexCount; i++)
	{
		indices[i] = i;
	}

	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(WindowWertex) * mVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &mVertexBuffer);
	if (FAILED(result))
	{
		return false;
	}

	// Set up the description of the index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * mIndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &mIndexBuffer);
	if (FAILED(result))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;

	return true;
}

void OrthographicWindow::Render(ID3D11DeviceContext* deviceContext) {
	// Set vertex buffer stride and offset.
	unsigned int stride = sizeof(WindowWertex);
	unsigned int offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}