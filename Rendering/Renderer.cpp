#include "Renderer.h"
#include "eSceneComp.h"
#include "DirectionalLight.h"

Renderer::Renderer(){
	mTextureMapShader = nullptr;
	mNormalMapShader = nullptr;
	mShadowMapShader = nullptr;
	mBlurShader = nullptr;
	mShadowPostShader = nullptr;
}

Renderer::~Renderer(){

}

bool Renderer::Initialize(ID3D11Device* device, HWND hWnd, unsigned int windowWidth, unsigned int windowHeight) {
	mFullWindow.reset(new OrthographicWindow);
	if (!mFullWindow->Initialize(device, windowWidth, windowHeight)) {
		return false;
	}

	mSmallWindow.reset(new OrthographicWindow);
	if (!mSmallWindow->Initialize(device, windowWidth / 2, windowHeight / 2)) {
		return false;
	}

	mDownSampleShader.reset(new DownSampleShader);
	if (!mDownSampleShader->Initialize(device, hWnd, windowWidth, windowHeight)) {
		return false;
	}
	
	mTextureMapShader.reset(new TextureMapShader);
	if (!mTextureMapShader->Initialize(device, hWnd, windowWidth, windowHeight)) {
		return false;
	}

	mNormalMapShader.reset(new NormalMapShader);
	if (!mNormalMapShader->Initialize(device, hWnd, windowWidth, windowHeight)) {
		return false;
	}

	mShadowMapShader.reset(new ShadowMapShader);
	if (!mShadowMapShader->Initialize(device, hWnd)) {
		return false;
	}

	mBlurShader.reset(new BlurShader);
	if (!mBlurShader->Initialize(device, hWnd, windowWidth, windowHeight)) {
		return false;
	}

	mShadowPostShader.reset(new ShadowPostProcessing);
	if (!mShadowPostShader->Initialize(device, hWnd)) {
		return false;
	}

	mWindowHeight = windowHeight;
	mWindowWidth = windowWidth;

	return true;
}

void Renderer::RenderScene(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, Direct3D* d3d, const DirectX::XMFLOAT4X4 view) {
	DirectX::XMFLOAT4X4 windowWorld;
	DirectX::XMStoreFloat4x4(&windowWorld, DirectX::XMMatrixInverse(nullptr, DirectX::XMLoadFloat4x4(&view)));
	windowWorld._41 += windowWorld._31 * 1.2f;
	windowWorld._42 += windowWorld._32 * 1.2f;
	windowWorld._43 += windowWorld._33 * 1.2f;
	
	RenderTextureMap(objects, d3d->mDeviceContext, view, d3d->mProjectionMatrix);
	RenderNormalMap(objects, d3d->mDeviceContext, view, d3d->mProjectionMatrix);
	RenderDepthMaps(objects, d3d->mDeviceContext, view, d3d->mProjectionMatrix);
	RenderLightedScene(objects, d3d->mDeviceContext, view, d3d->mProjectionMatrix);
	
	//Turn z-buffer off
	d3d->mDeviceContext->OMSetDepthStencilState(d3d->mDepthDisabledStencilState, 1);

	RenderShadowBlurMap(objects, d3d->mDeviceContext, windowWorld, view, d3d->mOrthographicMatrix);

	d3d->mDeviceContext->OMSetRenderTargets(1, &d3d->mRenderTargetView.p, d3d->mDepthStencilView.p);
	d3d->mDeviceContext->RSSetViewports(1, &d3d->mViewport);

	RenderShadowPostProcess(objects, d3d->mDevice, d3d->mDeviceContext, windowWorld, view, d3d->mOrthographicMatrix);

	RenderPostProcesses(d3d->mDeviceContext, windowWorld, view, d3d->mOrthographicMatrix);

	//Turn z-buffer on
	d3d->mDeviceContext->OMSetDepthStencilState(d3d->mDepthStencilState, 1);
}

void Renderer::RenderPostProcesses(ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 orthographic) {
	
	
}

void Renderer::RenderDepthMaps(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection) {
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator lightIter = objects->begin();

	while (lightIter != objects->end()) {
		auto lightComp = dynamic_cast<DirectionalLight*>(lightIter->second.get());

		if (lightComp) {

			lightComp->mDepthMap->SetRenderTarget(deviceContext);
			lightComp->mDepthMap->ClearRenderTarget(deviceContext, DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

			std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator sceneCompIter = objects->begin();

			while (sceneCompIter != objects->end()) {
				auto sceneComp = dynamic_cast<eSceneComp*>(sceneCompIter->second.get());

				if (sceneComp) {
					sceneComp->Render(deviceContext);
					mShadowMapShader->Render(deviceContext, sceneComp->mMesh->mIndexCount, sceneComp->mTransform, lightComp->mTransform, projection);
				}
				sceneCompIter++;
			}
		}
		lightIter++;
	}
}

void Renderer::RenderLightedScene(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection) {
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator lightIter = objects->begin();

	while (lightIter != objects->end()) {
		auto lightComp = dynamic_cast<DirectionalLight*>(lightIter->second.get());

		if (lightComp) {

			lightComp->mLightedSceneBuffer->SetRenderTarget(deviceContext);
			lightComp->mLightedSceneBuffer->ClearRenderTarget(deviceContext, DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

			std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator sceneCompIter = objects->begin();

			while (sceneCompIter != objects->end()) {
				auto sceneComp = dynamic_cast<eSceneComp*>(sceneCompIter->second.get());

				if (sceneComp) {
					sceneComp->Render(deviceContext);
					mDownSampleShader->Render(deviceContext, sceneComp->mMesh->mIndexCount, sceneComp->mTransform, view, projection, lightComp->mTransform, lightComp->mAmbientColor, lightComp->mDiffuseColor, lightComp->mIntensity, lightComp->mLightDirection, lightComp->mDepthMap->mShaderResourceView);
				}
				sceneCompIter++;
			}
		}
		lightIter++;
	}
}

void Renderer::RenderTextureMap(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection) {
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator sceneCompIter = objects->begin();

	mTextureMapShader->mTextureBuffer->ClearRenderTarget(deviceContext, DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	mTextureMapShader->mTextureBuffer->SetRenderTarget(deviceContext);

	while (sceneCompIter != objects->end()) {
		auto sceneComp = dynamic_cast<eSceneComp*>(sceneCompIter->second.get());

		if (sceneComp) {
			sceneComp->Render(deviceContext);
			mTextureMapShader->Render(deviceContext, sceneComp->mMesh->mIndexCount, sceneComp->mTransform, view, projection, sceneComp->mTexture->mTexture);
		}
		sceneCompIter++;
	}
}

void Renderer::RenderNormalMap(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection) {
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator sceneCompIter = objects->begin();
	
	mNormalMapShader->mNormalBuffer->ClearRenderTarget(deviceContext, DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	mNormalMapShader->mNormalBuffer->SetRenderTarget(deviceContext);

	while (sceneCompIter != objects->end()) {
		auto sceneComp = dynamic_cast<eSceneComp*>(sceneCompIter->second.get());

		if (sceneComp) {
			sceneComp->Render(deviceContext);
			mNormalMapShader->Render(deviceContext, sceneComp->mMesh->mIndexCount, sceneComp->mTransform, view, projection);
		}
		sceneCompIter++;
	}
}

void Renderer::RenderShadowBlurMap(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 orthographic){	
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator lightIter = objects->begin();

	while (lightIter != objects->end()) {
		auto lightComp = dynamic_cast<DirectionalLight*>(lightIter->second.get());

		if (lightComp) {
			mFullWindow->Render(deviceContext);

			lightComp->mDownSampleBuffer->ClearRenderTarget(deviceContext, DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
			lightComp->mDownSampleBuffer->SetRenderTarget(deviceContext);

			mTextureMapShader->Render(deviceContext, mSmallWindow->mIndexCount, world, view, orthographic, lightComp->mLightedSceneBuffer->mShaderResourceView);

			lightComp->mBlurBuffer->ClearRenderTarget(deviceContext, DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
			lightComp->mBlurBuffer->SetRenderTarget(deviceContext);

			mBlurShader->Render(deviceContext, mSmallWindow->mIndexCount, world, view, orthographic, (float)mWindowWidth, (float)mWindowHeight, lightComp->mDownSampleBuffer->mShaderResourceView);

			mFullWindow->Render(deviceContext);

			lightComp->mLightedSceneBuffer->ClearRenderTarget(deviceContext, DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
			lightComp->mLightedSceneBuffer->SetRenderTarget(deviceContext);

			mTextureMapShader->Render(deviceContext, mFullWindow->mIndexCount, world, view, orthographic, lightComp->mBlurBuffer->mShaderResourceView);
		}
		lightIter++;
	}
}

void Renderer::RenderShadowPostProcess(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, ID3D11Device* device, ID3D11DeviceContext* deviceContext, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 orthographic) {
	std::vector<ID3D11Texture2D*> downSamplesVec;
	HRESULT hresult;

	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator lightIter = objects->begin();

	while (lightIter != objects->end()) {
		auto lightComp = dynamic_cast<DirectionalLight*>(lightIter->second.get());

		if (lightComp) {
			downSamplesVec.push_back(lightComp->mLightedSceneBuffer->mRenderTargetTexture.p);
		}
		lightIter++;
	}

	D3D11_TEXTURE2D_DESC texArrayDesc;
	downSamplesVec[0]->GetDesc(&texArrayDesc);
	texArrayDesc.ArraySize = downSamplesVec.size();

	ID3D11Texture2D* texArray = NULL;
	hresult = device->CreateTexture2D(&texArrayDesc, NULL, &texArray);
	if (FAILED(hresult)) {
		return;
	}

	for (unsigned int i = 0; i < downSamplesVec.size(); i++) {
		for (unsigned int j = 0; j < texArrayDesc.MipLevels; j++) {
			//D3D11_MAPPED_SUBRESOURCE mappedResource;
			/*deviceContext->Map(downSamplesVec[i], j, D3D11_MAP_READ_WRITE, 0, &mappedResource);
			deviceContext->UpdateSubresource(texArray, D3D11CalcSubresource(j, i, texArrayDesc.MipLevels), 0, mappedResource.pData, mappedResource.RowPitch, 0);
			deviceContext->Unmap(downSamplesVec[i], j);*/
			const uint32_t subResourceIndex = D3D11CalcSubresource(j, 0, texArrayDesc.MipLevels);
			const uint32_t destinationSubresource = D3D11CalcSubresource(j, i, texArrayDesc.MipLevels);
			deviceContext->CopySubresourceRegion(texArray, static_cast<UINT> (destinationSubresource), 0, 0, 0, downSamplesVec[i], subResourceIndex, nullptr);
		}
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC arrDesc;
	arrDesc.Format = texArrayDesc.Format;
	arrDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	arrDesc.Texture2DArray.ArraySize = downSamplesVec.size();
	arrDesc.Texture2DArray.MostDetailedMip = 0;
	arrDesc.Texture2DArray.MipLevels = texArrayDesc.MipLevels;
	arrDesc.Texture2DArray.FirstArraySlice = 0;
	
	ID3D11ShaderResourceView* downSamplesArr;
	device->CreateShaderResourceView(texArray, &arrDesc, &downSamplesArr);

	mShadowPostShader->Render(deviceContext, mFullWindow->mIndexCount, world, view, orthographic, downSamplesVec.size(), &downSamplesArr, mTextureMapShader->mTextureBuffer->mShaderResourceView, mNormalMapShader->mNormalBuffer->mShaderResourceView);

	downSamplesArr->Release();
	downSamplesArr = 0;

	texArray->Release();
	texArray = 0;
}