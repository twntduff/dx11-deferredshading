#ifndef _ORTHOGRAPHICWINDOW_H_
#define _ORTHOGRAPHICWINDOW_H_

//Includes
#include <d3d11.h>
#include <DirectXMath.h>

struct WindowWertex {
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT2 Texture;
};

class OrthographicWindow
{
private:
	ID3D11Buffer* mVertexBuffer;
	ID3D11Buffer* mIndexBuffer;
	unsigned int mVertexCount;

public:
	unsigned int mIndexCount;

public:
	OrthographicWindow();
	~OrthographicWindow();

	bool Initialize(ID3D11Device* device, const unsigned int windowWidth, const unsigned int windowHeight);
	void Render(ID3D11DeviceContext* deviceContext);
};

#endif