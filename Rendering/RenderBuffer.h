#ifndef _RENDERBUFFER_H_
#define _RENDERBUFFER_H_

//Includes
#include <d3d11.h>
#include <DirectXMath.h>
#include <atlbase.h>

class RenderBuffer
{
public:
	ATL::CComPtr<ID3D11Texture2D> mRenderTargetTexture;
	ATL::CComPtr<ID3D11RenderTargetView> mRenderTargetView;
	ATL::CComPtr<ID3D11ShaderResourceView> mShaderResourceView;
	ATL::CComPtr<ID3D11Texture2D> mDepthStencilBuffer;
	ATL::CComPtr<ID3D11DepthStencilView> mDepthStencilView;
	D3D11_VIEWPORT mViewport;

public:
	RenderBuffer();
	~RenderBuffer();

	bool Initialize(ID3D11Device* device, const unsigned int textureWidth, const unsigned int textureHeight);
	void SetRenderTarget(ID3D11DeviceContext* deviceContext);
	void ClearRenderTarget(ID3D11DeviceContext* deviceContext, DirectX::XMFLOAT4 color);
};

#endif